﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;



namespace ejer01.Controllers
{
    [Route("/api/info")]
    [ApiController]
    public class InfoController : ControllerBase
    {
        private readonly IConfiguration _config;
        public InfoController(IConfiguration config)
        {
            _config = config;
        }

        [HttpGet]
        public string GetInfo()
        {
            string projectTitle = _config.GetSection("Project").GetSection("Title").Value;
            string envName = _config.GetSection("Project").GetSection("EnvironmentName").Value;
            string dbConnection = _config.GetConnectionString("Database");
            Console.Out.WriteLine($"We are in Project Title:{projectTitle}");
            Console.Out.WriteLine($"We are in Environment Name: {envName} ");
            Console.Out.WriteLine($"We are connecting to: {dbConnection}");

            return $"We are in Project Title = {projectTitle} and Environment Name ={envName}";
        }

    }
}
