﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;

namespace ejer01.Controllers
{
    [ApiController]
    [Route("/api/students")]
    public class StudentsController : ControllerBase
    {
        private readonly IConfiguration _config;
        public StudentsController(IConfiguration config)
        {
            _config = config;
        }

        [HttpGet]
        public List<Student> GetStudents()
        {
            string projectTitle = _config.GetSection("Project").GetSection("Title").Value;
            string dbConnection = _config.GetConnectionString("Database");
            Console.Out.WriteLine($"We are in env :{projectTitle}");
            Console.Out.WriteLine($"We are connecting to: {dbConnection}");

            return new List<Student>()
            {
                new Student() {Name = $"Pablo from env: {projectTitle}, " },
                new Student() {Name = "Monica" },
                new Student() {Name = "Juan" }
            };
        }

        


        [HttpPost]
        public Student CrateStudent([FromBody] string personName)
        {
            return new Student()
            {
                Name = personName
            };
        }

        [HttpPut]
        public Student UpateStudent([FromBody] Student student)
        {
            student.Name = "updated";
            return student;
        }

        [HttpDelete]
        public Student DeleteStudent([FromBody] Student student)
        {
            student.Name = "deleted";
            return student;
        }

    }
}




